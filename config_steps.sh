#! /bin/bash

#init venv -> can be updated
source "venv/bin/activate"

echo -e "\tDjango server configuration\nRun django-admin to test framework [Y/n]?"
read dec

# test if framework is correctly installed
if [[ $dec == 'y' || $dec == 'Y' ]]; then
 django-admin
fi

# check if src folder exists
if [[ -e "src" ]]; then
 echo "src folder exists, all its contents will be removed"
 rm -rf "src/"
fi
mkdir "src/"

# start django 
cd "src/"
echo "Currently at: $(pwd)"
django-admin startproject "rtsp_camera" . 
echo "Starting the server..."

# start server
python manage.py runserver


deactivate
echo -e "\tEnd of script"

