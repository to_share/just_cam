#! /bin/bash

source "venv/bin/activate"
cd "src"

# migrations
python manage.py migrate
python manage.py makemigrations

# create superuser
python manage.py createsuperuser

# startapp - should be done once
# python manage.py startapp camera

deactivate
