from django.db import models

# Create your models here.
class Cam(models.Model):
    ip = models.TextField(default='192.168.0.51')
    password = models.TextField(default='NSDTRU')
    ratio = models.FloatField(default=0.5)