from django.urls import path
from .views import main_view

app_name='Camera'
urlpatterns=[
    path('',main_view,name='camera-view'),
]