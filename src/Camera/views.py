from django.http import HttpResponse
from django.shortcuts import render
from django.core.mail import EmailMessage
from django.views.decorators import gzip
from django.http import StreamingHttpResponse
from .models import Cam
import cv2
import threading
from window.just_stream import Stream

@gzip.gzip_page
def main_view(request):
    try:
        obj = Cam.objects.all()[0]
        cam = VideoCamera(ip=str(obj.ip), passwd=str((obj.password)), ratio=obj.ratio)
        return StreamingHttpResponse(gen(cam), content_type="multipart/x-mixed-replace;boundary=frame")
    except:
        pass
    return render(request, 'Camera/camera_view.html')

#to capture video class
class VideoCamera(Stream):
    def __init__(self,ip='192.168.0.15', passwd='NSDTRU', ratio=0.5):
        super().__init__(ip,passwd)
        self.ratio = ratio
        (self.grabbed, self.frame) = self.cap.read()
        threading.Thread(target=self.update, args=()).start()

    def __del__(self):
        self._quit()

    def ret_frame(self):
        image = self.frame
        image =  cv2.resize(image, (0,0), fx=self.ratio, fy=self.ratio)
        _, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()

    def update(self):
        while True:
            (self.grabbed, self.frame) = self.cap.read()

def gen(camera):
    while True:
        frame = camera.ret_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
