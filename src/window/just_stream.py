import cv2
import time

class Stream(object):

    def __init__(self, ip='192.168.0.51', passwd='NSDTRU', ratio=1):
        self.ip = ip
        self.passwd = passwd
        addr = f'rtsp://admin:{passwd}@{ip}/h264_stream'
        self.ratio = ratio
        self.cap = cv2.VideoCapture(addr)

    def get_frame(self):
        ret, frame = self.cap.read()
        frame = cv2.resize(frame, (0,0), fx=self.ratio, fy=self.ratio)
        return frame

    def display(self,time):
        try:
            while True:
                cv2.imshow('Returned frames', self.get_frame())
                cv2.waitKey(time)
        except KeyboardInterrupt as e:
            print('Ctrl+c pressed!')
            self._quit()
        except:
            self.display(time)


    def _quit(self):
        self.cap.release()
        cv2.destroyAllWindows()

if __name__ == '__main__':
    s = Stream(ip='192.168.0.15', ratio=0.5)
    s.display(1)