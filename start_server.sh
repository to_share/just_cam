#! /bin/bash

echo -e "\tNow server will be started on port 8000"
source "venv/bin/activate"
cd "src/"
python manage.py runserver 0.0.0.0:8000
